let canvas = document.querySelector('canvas');
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

let c = canvas.getContext('2d');

let snow = [];

(function draw(){
	setTimeout(function(){
		requestAnimationFrame(draw);
		console.log(snow)
		
		c.clearRect(0, 0, innerWidth, innerHeight);

		let myFlake = new Flake();
		snow.push(myFlake);

		for(flake of snow) {
			flake.render();
			flake.update();
		}

		for(let i = snow.length -1; i >= 0; i--){
			if(snow[i].posY > window.innerHeight + 10){
				snow.splice(i, 1);
			}
		}
	}, 9000 / 500);
})();
