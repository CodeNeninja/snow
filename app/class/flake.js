class Flake {
	constructor (){
		this.posX = Math.floor((Math.random() * window.innerWidth));
		this.size = Math.floor((Math.random() * 10) +1);
		this.posY = 0 - this.size;
	}

	render() {
		c.beginPath();
		c.arc(this.posX, this.posY, this.size, 0, Math.PI * 2, false);
		c.fillStyle = "white";
		c.strokeStyle = "white";
		c.fill();
		c.stroke();
	}

	getCurrentLocation() {
		return this.posY;
	}

	update() {
		switch(this.size) {
			case 1: this.posY += 1; break
			case 2: this.posY += 1.5; break
			case 3: this.posY += 0.75; break
			case 4: this.posY += 1; break
			case 5: this.posY += 1.25; break
			case 6: this.posY += 1.5; break
			case 7: this.posY += 1.75; break
			case 8: this.posY += 2; break
			case 9: this.posY += 2.25; break
			case 10: this.posY += 2.5; break
		}
	}
}
